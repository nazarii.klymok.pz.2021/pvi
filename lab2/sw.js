// Install service worker
self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open('my-pwa-cache')
        .then((cache) => {
            return cache.addAll([
                'http://localhost:5500/lab2/',
                'http://localhost:5500/lab2/index.html',
                'http://localhost:5500/lab2/styles.css',
                'http://localhost:5500/lab2/scripts.js',
                'http://localhost:5500/lab2/student.js',
                'http://localhost:5500/lab2/img/cms.png',
                'http://localhost:5500/lab2/img/profile.png',
                'http://localhost:5500/lab2/img/bell.png'
            ]);
        })
    );
});

// Fetch resources with cache fallback
self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
        .then((response) => {
            if (response) {
                return response;
            }
            return fetch(event.request)
                .then((response) => {
                    return caches.open('my-pwa-cache')
                        .then((cache) => {
                            cache.put(event.request, response.clone());
                            return response;
                        });
                });
        })
    );
});

// Update cache when new service worker is activated
self.addEventListener('activate', (event) => {
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return Promise.all(
                cacheNames.filter((cacheName) => {
                    return cacheName.startsWith('my-pwa-cache-') &&
                        cacheName !== 'my-pwa-cache-v1';
                }).map((cacheName) => {
                    return caches.delete(cacheName);
                })
            );
        })
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request)
        .then((response) => {
            if (response) {
                return response;
            }
            return fetch(event.request);
        }),
    );
});
