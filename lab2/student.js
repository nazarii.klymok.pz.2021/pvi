export class Student {
    constructor(id, group, firstName, lastName, gender, bdate) {
        this.id = id;
        this.group = Object.is(group, undefined) ? "" : group;
        this.firstName = Object.is(firstName, undefined) ? "" : firstName;
        this.lastName = Object.is(lastName, undefined) ? "" : lastName;
        this.gender = Object.is(gender, undefined) ? "" : gender;
        this.bdate = Object.is(bdate, undefined) ? "" : bdate;
    }
}