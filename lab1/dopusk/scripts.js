setTimeout(red, 1000);

function red() {
    document.getElementById("car").remove();
    createCar();
    document.getElementById("green").style.setProperty("background-color", "gray");
    document.getElementById("yellow").style.setProperty("background-color", "gray");
    document.getElementById("red").style.setProperty("background-color", "red");
    setTimeout(yellow, 3000);
}

function yellow() {
    document.getElementById("green").style.setProperty("background-color", "gray");
    document.getElementById("red").style.setProperty("background-color", "gray");
    document.getElementById("yellow").style.setProperty("background-color", "yellow");
    setTimeout(green, 3000);
}

function green() {
    document.getElementById("red").style.setProperty("background-color", "gray");
    document.getElementById("yellow").style.setProperty("background-color", "gray");
    document.getElementById("green").style.setProperty("background-color", "green");
    startCar();
}

function createCar() {
    let div = document.createElement("div");
    let img = document.createElement("img");
    img.src = "https://static.vecteezy.com/system/resources/previews/001/193/930/original/vintage-car-png.png"
    img.setAttribute("width", "100px");
    div.id = "car";
    div.appendChild(img);
    document.getElementById("road").appendChild(div);
}

function startCar() {
    document.getElementById("car").classList.add("moving");
    setTimeout(red, 3000);
}