function createItem() {
    let itemName = document.getElementById("txtItemName").value;
    let imgLink = document.getElementById("txtItemImgLink").value;
    let shoplist = document.getElementById("shoplist");
    let newItem = document.createElement("li");

    let checkbox = document.createElement("input");
    checkbox.className = "itemCheckbox"
    checkbox.type = "checkbox";

    newItemLink = document.createElement("a");
    newItemLink.className = "no-color";
    newItemLink.href = imgLink;
    newItemLink.appendChild(document.createTextNode(itemName));

    newItem.append(checkbox);
    newItem.appendChild(newItemLink);

    shoplist.appendChild(newItem);
    console.log("done")
}

function deleteChecked() {
    let checkboxes = document.getElementsByClassName("itemCheckbox");
    let checkboxesToDelete = [];
    for (let element of checkboxes) {
        if (element.checked) {
            checkboxesToDelete.push(element.parentElement);
        }
    }
    for (let toDelete of checkboxesToDelete) {
        toDelete.remove();
    }
}