// Get the modal
var modalAddStudent = document.getElementById("modal_add_student");
var modalDeleteStudent = document.getElementById("modal_delete_student");
var modalDeletePrompt = document.getElementById("delete_prompt");
// Get the button that opens the modal
var btnAddStudent = document.getElementById("btn_add_student");
var curr_delete_student = -1;
var student_id_increment = 2;

function showNotificationCount() {
    let badge = document.getElementById("notification_counter");
    if (badge.classList.contains("badge_invisible")) {
        badge.classList.remove("badge_invisible");
    } else {
        badge.classList.add("badge_invisible");
    }
}

function getStudent(id) {
    var studentRows = document.getElementById("students_table").getElementsByClassName("student-row");
    for (var i = 0; i < studentRows.length; i++) {
        console.log(studentRows.item(i).getAttribute("arg-id"));
        if (studentRows.item(i).getAttribute("arg-id") == id)  {
            return studentRows.item(i);
        }
    }
}

function doDeleteStudent() {
    getStudent(curr_delete_student).remove();
    showDeleteStudentModal(false);
}

function deleteStudent(id) {
    curr_delete_student = id;
    const studentName = getStudent(curr_delete_student).children.item(2).innerHTML;
    modalDeletePrompt.innerHTML = `Are you sure you want to delete user ${studentName}?`;
    showDeleteStudentModal(true);
}

function checkAll() {
    Array.from(document.getElementById("students_table").getElementsByClassName("student-checkbox"))
        .forEach(element => element.checked = document.getElementById("checkbox_all").checked);
}


btnAddStudent.onclick = function() {
    showAddStudentModal(true);
}

window.onclick = function(event) {
  if (event.target == modalAddStudent) {
    showAddStudentModal(false);
  }
}

function showAddStudentModal(show) {
    modalAddStudent.style.display = show ? "block" : "none";
}

function showDeleteStudentModal(show) {
    modalDeleteStudent.style.display = show ? "block" : "none";
}

function doCreateStudent(group, fname, lname, gender, bday) {
    let tr = document.createElement("tr");
    let tdCheckbox = document.createElement("td");
    let checkbox = document.createElement("input");
    let tdGroup = document.createElement("td");
    let tdFNameLname = document.createElement("td");
    let tdGender = document.createElement("td");
    let tdBday = document.createElement("td");
    let tdIndicator = document.createElement("td");
    let spanIndicator = document.createElement("span");
    let tdButtons = document.createElement("td");
    let divButtons = document.createElement("div");
    let btnEdit = document.createElement("button");
    let imgEdit = document.createElement("i");
    let btnDelete = document.createElement("button");
    let imgDelete = document.createElement("i");
    tr.classList.add("student-row");
    tr.setAttribute("arg-id", student_id_increment);
    tdCheckbox.appendChild(checkbox);
    checkbox.type = "checkbox";
    checkbox.classList.add("student-checkbox");
    tdGroup.innerText = group;
    tdFNameLname.innerText = fname + " " + lname;
    tdGender.innerText = gender;
    tdBday.innerText = bday;
    tdIndicator.appendChild(spanIndicator);
    spanIndicator.classList.add("indicator", "gray");
    tdButtons.appendChild(divButtons);
    divButtons.classList.add("table_buttons");
    divButtons.appendChild(btnEdit);
    divButtons.appendChild(btnDelete); 
    btnDelete.classList.add("table_button");
    btnDelete.appendChild(imgDelete);
    imgDelete.classList.add("fa", "fa-close");
    btnEdit.classList.add("table_button");
    btnEdit.appendChild(imgEdit);
    imgEdit.classList.add("fa", "fa-pencil");
    btnDelete.setAttribute("onclick", `deleteStudent(${student_id_increment})`)
    tr.append(tdCheckbox, tdGroup, tdFNameLname, tdGender, tdBday, tdIndicator, tdButtons);
    document.getElementById("students_table").getElementsByTagName("tbody").item(0).appendChild(tr);
}

function createStudent() {
    const group = document.getElementById("txt_student_group").value;
    const fname = document.getElementById("txt_student_fname").value;
    const lname = document.getElementById("txt_student_lname").value;
    const gender = document.getElementById("txt_student_gender").value;
    const bday = document.getElementById("txt_student_bdate").value;
    doCreateStudent(group, fname, lname, gender, bday);
    student_id_increment++;
    showAddStudentModal(false);
}