import { Student } from "./student.js";

window.alterStudent = alterStudent;
window.showAlterStudentModal = showAlterStudentModal;
window.showDeleteStudentModal = showDeleteStudentModal;
window.doDeleteStudent = doDeleteStudent;
window.submitForm = submitForm;
window.checkAll = checkAll;
window.deleteStudent = deleteStudent;
window.showNotificationCount = showNotificationCount;
// Get the modal
var modalAddStudent = document.getElementById("modal_alter_student");
var modalDeleteStudent = document.getElementById("modal_delete_student");
var modalDeletePrompt = document.getElementById("delete_prompt");
// Get the button that opens the modal
var btnAddStudent = document.getElementById("btn_add_student");
var modalStudentHeader = document.getElementById("modal_student_header");
var modalStudentSubmitButton = document.getElementById("modal_student_submit_button");
var invisibleStudentId = document.getElementById("invisible_student_id");
var student_id_increment = 0;
var isEditMode = false;
var students = {};

$("#student_form").submit(function(e) {
    e.preventDefault();
    alterStudent();
})

function showNotificationCount() {
    let badge = document.getElementById("notification_counter");
    if (badge.classList.contains("badge_invisible")) {
        badge.classList.remove("badge_invisible");
    } else {
        badge.classList.add("badge_invisible");
    }
}

function getStudent(id) {
    return students[id];
}

function doDeleteStudent() {
    delete students[getCurrentStudentId()];
    showDeleteStudentModal(false);
    displayStudents();
}

function deleteStudent(id) {
    setCurrentStudentId(id);
    let student = getStudent(id);
    const studentName = student.firstName + ' ' + student.lastName;
    modalDeletePrompt.innerHTML = `Are you sure you want to delete user ${studentName}?`;
    showDeleteStudentModal(true);
}

function getCurrentStudentId() {
    return invisibleStudentId.dataset.currentStudentId;
}

function setCurrentStudentId(id) {
    invisibleStudentId.dataset.currentStudentId = id;
}

function checkAll() {
    Array.from(document.getElementById("students_table").getElementsByClassName("student-checkbox"))
        .forEach(element => { 
            element.checked = document.getElementById("checkbox_all").checked;
            element.dispatchEvent(new Event('change'));
         });
}


btnAddStudent.onclick = function() {
    setEditMode(false);
    resetStudentModalValues();
    showAlterStudentModal(true);
}

window.onclick = function(event) {
  if (event.target == modalAddStudent) {
    showAlterStudentModal(false);
  }
}

function showAlterStudentModal(show) {
    modalAddStudent.style.display = show ? "block" : "none";
}

function showDeleteStudentModal(show) {
    modalDeleteStudent.style.display = show ? "block" : "none";
}

function setStudentModalValues(studentId) {
    let student = getStudent(studentId);
    document.getElementById("txt_student_group").value = student.group;
    document.getElementById("txt_student_fname").value = student.firstName;
    document.getElementById("txt_student_lname").value = student.lastName;
    document.getElementById("txt_student_gender").value = student.gender;
    document.getElementById("txt_student_bdate").value = student.bdate;
}

function resetStudentModalValues() {
    document.getElementById("txt_student_group").value = '';
    document.getElementById("txt_student_fname").value = '';
    document.getElementById("txt_student_lname").value = '';
    document.getElementById("txt_student_gender").value = '';
    document.getElementById("txt_student_bdate").value = '';
}

function createStudent() {
    const group = document.getElementById("txt_student_group").value;
    const fname = document.getElementById("txt_student_fname").value;
    const lname = document.getElementById("txt_student_lname").value;
    const gender = document.getElementById("txt_student_gender").value;
    const bday = document.getElementById("txt_student_bdate").value;
    students[student_id_increment] = new Student(student_id_increment, group, fname, lname, gender, bday);
    console.log(JSON.stringify(students[student_id_increment++]));
}

// print whole table at once, don't modify one row
function editStudent(studentId) {
    const group = document.getElementById("txt_student_group").value;
    const fname = document.getElementById("txt_student_fname").value;
    const lname = document.getElementById("txt_student_lname").value;
    const gender = document.getElementById("txt_student_gender").value;
    const bday = document.getElementById("txt_student_bdate").value;
    students[studentId] = new Student(studentId, group, fname, lname, gender, bday);
    console.log(JSON.stringify(students[studentId]));
}

function displayStudents() {
    Array.from(
        document.getElementById("students_table").getElementsByTagName("tbody").item(0).children
    ).slice(1).forEach(row => row.innerHTML = "");
    for (var student in students) {
        let studentId = student;
        let tr = document.createElement("tr");
        let tdCheckbox = document.createElement("td");
        let checkbox = document.createElement("input");
        let tdGroup = document.createElement("td");
        let tdFNameLname = document.createElement("td");
        let tdGender = document.createElement("td");
        let tdBday = document.createElement("td");
        let tdIndicator = document.createElement("td");
        let spanIndicator = document.createElement("span");
        let tdButtons = document.createElement("td");
        let divButtons = document.createElement("div");
        let btnEdit = document.createElement("button");
        let imgEdit = document.createElement("i");
        let btnDelete = document.createElement("button");
        let imgDelete = document.createElement("i");
        tr.classList.add("student-row");
        tr.setAttribute("arg-id", studentId);
        tdCheckbox.appendChild(checkbox);
        checkbox.type = "checkbox";
        checkbox.classList.add("student-checkbox");
        checkbox.addEventListener('change', event => {
            if (checkbox.checked && !spanIndicator.classList.contains('green')) {
                spanIndicator.classList.add('green');
            } else if (!checkbox.checked) {
                spanIndicator.classList.remove('green');
            }
        })
        tdGroup.innerText = students[studentId].group;
        tdFNameLname.innerText = students[studentId].firstName + " " + students[studentId].lastName;
        tdGender.innerText = students[studentId].gender;
        tdBday.innerText = students[studentId].bdate;
        tdIndicator.appendChild(spanIndicator);
        spanIndicator.classList.add("indicator", "gray");
        tdButtons.appendChild(divButtons);
        divButtons.classList.add("table_buttons");
        divButtons.appendChild(btnEdit);
        divButtons.appendChild(btnDelete); 
        btnDelete.classList.add("table_button");
        btnDelete.appendChild(imgDelete);
        imgDelete.classList.add("fa", "fa-close");
        btnEdit.classList.add("table_button");
        btnEdit.appendChild(imgEdit);
        imgEdit.classList.add("fa", "fa-pencil");
        btnDelete.setAttribute("onclick", `deleteStudent(${studentId})`);
        btnEdit.onclick = function() {
            setCurrentStudentId(studentId);
            setEditMode(true);
            setStudentModalValues(studentId);
            showAlterStudentModal(true);
        }
        tr.append(tdCheckbox, tdGroup, tdFNameLname, tdGender, tdBday, tdIndicator, tdButtons);
        document.getElementById("students_table").getElementsByTagName("tbody").item(0).appendChild(tr);
    }
}

function alterStudent() {
    if (isEditMode) {
        editStudent(getCurrentStudentId());
        
    } else {
        createStudent();
    }
    showAlterStudentModal(false);
    displayStudents();
}

function setEditMode(editMode) {
    if (editMode) {
        modalStudentHeader.innerHTML = "Edit student";
        modalStudentSubmitButton.title = "Save";
        isEditMode = true;
    } else {
        modalStudentHeader.innerHTML = "Add student";
        modalStudentSubmitButton.title = "Create";
        isEditMode = false;
    }
}

function submitForm(event) {
    event.preventDefault();
    alterStudent();
    return false;
}